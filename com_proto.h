/*
 * com_proto.h
 *
 *  Created on: 13-Dec-2017
 *      Author: aksonlyaks
 */

#ifndef COM_PROTO_H_
#define COM_PROTO_H_
#include <stdint.h>

#define START_BYTE			0xA5
#define END_BYTE			0x5A
#define true				1
#define false				0
#define ENABLE				true
#define DISABLE				false

typedef struct
{
  /* send one byte of data */
  int (*put_c)(char value);
  /* Get one byte */
  char (*get_c)(void);
  /* Receive buffer not empty */
  uint8_t (*is_rx_ready)(void);
} t_serial_if;

extern t_serial_if serial_if;


struct config_data_t{
	uint32_t cConfigVersion;						// Configuration version
	uint8_t cFwVersion[24];							// Device firmware version in present in device; Read ONLY
	uint8_t cSonicTid[16];							// Sonic TID
	uint8_t cUSonicTid[16];							// USonic TID
	uint8_t cDevPwd[4];								// Device Password
	uint32_t cDevNo[4];								// Device serial Number; Read Only
	uint32_t cSamplingTick;							// Sampling Ticks value; Changes device behaviour
	uint32_t cSonicTick;							// Sonic generator tick; Changes device behaviour
	uint32_t cUltraSonicTick;						// USonic generator tick; Changes device behaviour
	uint32_t cSetAmnt;								// Set fixed amount value; Use with cFixedAmntMode
	uint32_t dDaysTxnAmnt;							// Stores past 24 hours txn sum data
	uint32_t dTotalTxnAmnt;							// Store device txn sum history
	uint32_t dDeviceBoots;							// Stores device number of boots
	uint8_t valid;									// Valid configuration indicator; To make default configuration change it to 0
	uint8_t cBurstEnableOnUSB;						// Enable Busrt Ultrasonic mode on USB connect
	uint8_t cBurstInt;								// Interval in seconds of Burst Ultrasonic mode
	uint8_t cEnableTap;								// Enable Tap feature
	uint8_t cEnableConfirm;							// Enable confirmation feature
	uint8_t cFixedAmntMode;							// Enable fixed amount mode
	uint8_t cEnableIVR;								// Enable IVR mode
	uint8_t cEnableWifi;							// Enable Wifi mode
	uint8_t cEnableKBD;								// Enable KeyBoard
	uint8_t cEnableMICMode;							// Enable Mic mode
};

#define ARG_LEN				sizeof(struct config_data_t)

struct config_struct{
	uint8_t data_type;
	uint8_t	sub_data_type;
	uint16_t length;
	uint8_t arg[ARG_LEN];
};
typedef struct config_struct config_setting_t;

typedef enum DATA_TYPE{
	DATA_T = 0,
	CONFIG_T,
	CMD_T,
	ACK_T,
	DEBUG_T,
	} data_type_t;

typedef enum CONFIG_ITEMS {
	SONIC_TID = 0,
	USONIC_TID,
	BURST_INTERVAL,
	ENABLE_BURST_ON_USBPLUG,
	TAP_ENABLE,
	CONFIRM_ENABLE,
	WIFI_ENABLE,
	FIXED_AMNT_SET,
	KEYBOARD_ENABLE,
	FEATURE_PHONE_ENABLE,
	DEVICE_PASSWORD,
	DEVICE_NUMBER,
	CONFIG_VERSION,
	FW_VERSION,
	CONFIG_READ,
	CONFIG_WRITE,
	CONFIG_END		= 240,
	}config_items_t;

typedef enum CMD_ITEM {
	TONE_PLAY = 0,
	USONIC_PLAY,
	START_MIC,
	SLEEP,
}cmd_item_t;

typedef enum DATA_ITEM {
	AMOUNT = 0,
	REF_CODE,
}data_item_t;

typedef enum ACK_ITEM {
	ACK_OK = 0,
	ACK_FAIL = 245,
	ACK_TIMED_OUT,
	ACK_RETRY,
	ACK_ARG_LEN,
	ACK_CONFIG_LEN,
	ACK_CRC_FAIL
	}ack_item_t;



/*****************   Function Declaration ******************/
/*
 * All these function need config_setting pointer variable as its first parameter.
 * This variable should be initialized and then passed to these functions.
 * Since all these functions modify the configuration already present in the device, hence
 * first get_config() should be called to get the current configuration from the device and then
 * and modification should be done.
 */
/*
 * Sets the TID for the SONIC tone
 */
void set_sonic_TID(config_setting_t *pod_config, char *TID);

/*
 * Sets the TID for Ultra Sonic tone
 */
void set_usonic_TID(config_setting_t *pod_config, char *TID);

/*
 * Sets device password: 4 Digits only
 * Before setting the password, old password should be confirmed from the user
 */
void set_dev_pwd(config_setting_t *pod_config, char *data);

/*
 * Sets the configuration version
 * Each time we update the configuration we change the version as well
 * Currently the version number is the current time in seconds
 */
void set_config_version(config_setting_t *pod_config);

/*
 * Sets if the configuration is valid or not
 * Setting the value of the valid byte other 1 makes it invalid
 */
void set_config_validity(config_setting_t *pod_config, uint8_t data);

/*
 * Enables burst UltraSonic enable
 * It requires additional parameter when time in seconds for the interval between two
 * consecutive bursts
 */
void set_burst_enable_on_usb(config_setting_t *pod_config, uint8_t state, uint8_t time_in_sec);

/*
 * Enable Tap feature
 */
void set_enable_tap(config_setting_t *pod_config, uint8_t state);

/*
 * Enable confirmation Feature
 */
void set_enable_confimration(config_setting_t *pod_config, uint8_t state);

/*
 * Enable Fixed amount mode
 * This mode enables to fix the amount whenever tap occurs
 */
void set_enable_fixed_amount_mode(config_setting_t *pod_config, uint8_t state, uint32_t amnt_in_paisa);

/*
 * Enables IVR feature
 */
void set_enable_IVR(config_setting_t *pod_config, uint8_t state);

/*
 * Enables Wifi
 */
void set_enable_wifi(config_setting_t *pod_config, uint8_t state);

/*
 * Enables KeyBoard
 */
void set_enable_kbd(config_setting_t *pod_config, uint8_t state);

/*
 * Enables MIC mode
 * This feature will make device to act as audio MIC device.
 */
void set_enable_mic_mode(config_setting_t *pod_config, uint8_t state);

/*
 * This function will write the modified configuration to the device
 * It returns 0 on success with updated configuration from the device in config variable
 * other wise return the error code
 */
uint8_t write_config(config_setting_t *config);

/*
 * Gets the configuration from the device
 * This function must be called before calling the
 */
char get_config(config_setting_t *config);

#endif /* COM_PROTO_H_ */

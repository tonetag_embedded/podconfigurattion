/*
 * com_proto.c
 *
 *  Created on: 04-Jan-2018
 *      Author: aksonlyaks
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "com_proto.h"

// To enable debug logs uncomment below #define
#define DEBUG

uint16_t CRC16native_c(char data[],int length, int start_pos) {
	char bt;
	int32_t wCrc = 0x6363;
	int idx = start_pos;
	int len= idx + length;
	do {
		bt = data[idx];
		idx++;

		bt = (char)(((bt & (0xFF)) ^ (wCrc & 0xFF)) & (0xFF));
		bt = (char)(((bt & (0xFF)) ^ ((bt << 4) & (0xFF))) & (0xFF));
		wCrc = (((wCrc) >> 8) & (0x00FFFFFF)) ^ ((bt & (0xFF)) << 8) ^ ((bt & (0xFF)) << 3) ^
		(((bt & (0xFF)) >> 4) & (0x0FFFFFFF));
	} while (idx < len);

	return wCrc;
}

void set_sonic_TID(config_setting_t *pod_config, char *TID)
{
	struct config_data_t *d_config;
	int TIDlen = 0;

	d_config = (struct config_data_t *)pod_config->arg;

	TIDlen = strlen(TID);
	if(strlen(TID) > sizeof(d_config->cSonicTid))
		TIDlen = sizeof(d_config->cSonicTid);

	memset(&d_config->cSonicTid, 0, sizeof(d_config->cSonicTid));
	memcpy(&d_config->cSonicTid, TID, TIDlen);
}

void set_usonic_TID(config_setting_t *pod_config, char *TID)
{
	struct config_data_t *d_config;
	int TIDlen = 0;

	d_config = (struct config_data_t *)pod_config->arg;

	TIDlen = strlen(TID);
	if(strlen(TID) > sizeof(d_config->cUSonicTid))
		TIDlen = sizeof(d_config->cUSonicTid);

	memset(&d_config->cUSonicTid, 0, sizeof(d_config->cUSonicTid));
	memcpy(&d_config->cUSonicTid, TID, TIDlen);
}

void set_dev_pwd(config_setting_t *pod_config, char *data)
{
	struct config_data_t *d_config;
	int PWDlen = 0;

	d_config = (struct config_data_t *)pod_config->arg;

	PWDlen = strlen(data);
	if(strlen(data) > sizeof(d_config->cDevPwd))
		PWDlen = sizeof(d_config->cDevPwd);

	memset(&d_config->cDevPwd, 0, sizeof(d_config->cDevPwd));
	memcpy(&d_config->cDevPwd, data, PWDlen);
}

void set_config_version(config_setting_t *pod_config)
{
	struct config_data_t *d_config;
	time_t rawtime;

	time(&rawtime);

	d_config = (struct config_data_t *)pod_config->arg;

	d_config->cConfigVersion = 0;
	d_config->cConfigVersion = (uint32_t)rawtime;
}

void set_config_validity(config_setting_t *pod_config, uint8_t data)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	memcpy(&d_config->valid, (const void *)&data, sizeof(d_config->valid));
}

void set_burst_enable_on_usb(config_setting_t *pod_config, uint8_t state, uint8_t time_in_sec)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	if(state == ENABLE)
	{
		d_config->cBurstInt = time_in_sec;
	}
	else if(state == DISABLE)
	{
		d_config->cBurstInt = 0;
	}
	d_config->cBurstEnableOnUSB = state;
}

void set_enable_tap(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableTap = state;
}

void set_enable_confimration(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableConfirm = state;
}

void set_enable_fixed_amount_mode(config_setting_t *pod_config, uint8_t state, uint32_t amnt_in_paisa)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cFixedAmntMode = state;
	d_config->cSetAmnt = amnt_in_paisa;
}

void set_enable_IVR(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableIVR = state;
}

void set_enable_wifi(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableWifi = state;
}

void set_enable_kbd(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableKBD = state;
}

void set_enable_mic_mode(config_setting_t *pod_config, uint8_t state)
{
	struct config_data_t *d_config;

	d_config = (struct config_data_t *)pod_config->arg;
	d_config->cEnableMICMode = state;
}

void send_data(config_setting_t *data_tx)
{
	int tx_len, i;
	uint8_t t_data;
	uint8_t *s_data = (uint8_t *)data_tx;
	uint16_t crc = 0;

	tx_len = data_tx->length + 4;							// length of argument + 3 bytes of config_setting

	if(data_tx->length != 0)
		crc = CRC16native_c((char *)&data_tx->arg, data_tx->length, 0);
#ifdef DEBUG
	printf("CRC:%x, len:%d\n", crc, data_tx->length);
#endif
	// Send start byte
	t_data = START_BYTE;
	serial_if.put_c(t_data);

	for(i = 0; i < tx_len; i++){
		serial_if.put_c(s_data[i]);
#ifdef DEBUG
		printf("%x ", s_data[i]);
#endif
	}
	if(data_tx->length != 0)
	{
		serial_if.put_c((char)crc & 0xFF);
		serial_if.put_c((char)(crc >> 8) & 0xFF);
	}
	//Send End byte
	t_data = END_BYTE;
	serial_if.put_c(t_data);
}

char read_data(config_setting_t *config_var)
{
	unsigned char s_data, i;
	int len;
	uint16_t crc_rx, crc_calc;

	do{
		s_data = serial_if.get_c();
#ifdef DEBUG
		printf("%c", s_data);
#endif
	}
	while(s_data != START_BYTE);

	if(s_data == START_BYTE)
	{
		printf("Start byte received\n");
		config_var->data_type = serial_if.get_c();
		config_var->sub_data_type = serial_if.get_c();
		i = serial_if.get_c();
		len = i;
		i = serial_if.get_c();
		len |= (i << 8);

		config_var->length = len;
#ifdef DEBUG
		printf("data_t:%x, sub_d:%x, len:%d\n", config_var->data_type, config_var->sub_data_type, config_var->length);
#endif
		i = 0;
		while(i < config_var->length)
		{
			s_data = serial_if.get_c();
			config_var->arg[i++] = s_data;
#ifdef DEBUG
			printf("%x ", config_var->arg[i - 1]);
#endif
		}
		if(config_var->length != 0)
		{
			crc_rx = serial_if.get_c() & 0xFF;
			crc_rx |= (serial_if.get_c() << 8) & 0xFF00;
			crc_calc = CRC16native_c((char *)config_var->arg, config_var->length, 0);
			printf("\n RX CRC:%x CALC CRC:%x\n", crc_rx, crc_calc);

			if(crc_rx != crc_calc)
			{
				printf("CRC match fail\n");
				return -2;
			}
		}

		printf("\n");
		return 0;
	}
	else
		return -1;
}

data_type_t read_ack(config_setting_t *config)
{
	printf("Response\n");
	read_data(config);

	if(config->data_type == ACK_T)
	{
		switch(config->sub_data_type)
		{
			case ACK_OK:
				printf("Message received successfully\n");
			break;

			case ACK_FAIL:
				printf("Message send fail\n");
			break;

			case ACK_RETRY:
				printf("Retry\n");
			break;

			case ACK_TIMED_OUT:
				printf("Message send timed out\n");
			break;

			case ACK_ARG_LEN:
				printf("Argument length wrong\n");
			break;

			case ACK_CONFIG_LEN:
				printf("COnfiguration length wrong\n");
			break;

			default:
			break;
		}
	}

	return config->sub_data_type;
}

char get_config(config_setting_t *config)
{
	int ret;

	config->data_type = CONFIG_T;
	config->sub_data_type = CONFIG_READ;
	config->length = 0;

	send_data(config);

	ret = read_data(config);
	if((ret == 0) && (config->data_type == CONFIG_T))
		return 0;
	else
		return (char)ACK_FAIL;
}

uint8_t write_config(config_setting_t *config)
{
	uint8_t ret;

	config->data_type = CONFIG_T;
	config->length = sizeof(struct config_data_t);
	config->sub_data_type = CONFIG_WRITE;

	send_data(config);

	memset(config, 0, sizeof(config_setting_t));
	ret = (uint8_t)read_ack(config);

	if(ret == ACK_OK)
	{
		get_config(config);
	}
	return ret;
}


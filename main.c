/*
 * main.c
 *
 *  Created on: 13-Dec-2017
 *      Author: aksonlyaks
 */
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include "com_proto.h"
#include <time.h>

#define serial_send(a)                                     	// Transmit char a
#define serial_rx_ready()       1												// Receiver full
#define serial_get(buff)        						// Receive char from modem


#define TID_DATA 	"D75789991"
unsigned int usb_fd;

int put_c(char c)
{
	int ret;

	ret = write(usb_fd, &c, 1);

	return ret;
}

char get_c(void)
{
	char buff;

	read(usb_fd, &buff, 1);

	return buff;
}

uint8_t serial_ready(void)
{
	return 1;
}

t_serial_if serial_if = {
		.put_c = put_c,
		.get_c = get_c,
		.is_rx_ready = serial_ready
};

struct config_data_t *d_config;

/* Serial intiailisation in PC*/
int serialInit(char *tty_port)
{
    struct termios  tty;
    struct termios  savetty;
    speed_t     spd;
    int    sfd, rc;

    sfd = open (tty_port, O_RDWR|O_NOCTTY); //|O_NONBLOCK);
	if(sfd < 0){
		perror("Opening:");
	}
	rc = tcgetattr(sfd, &tty);
    if (rc < 0) {
                printf("Issue in getting attributes:%s\n", tty_port);
       return -2;
    }
    savetty = tty;    /* preserve original settings for restoration */
    spd = B115200;
    cfsetospeed(&tty, (speed_t)spd);
    cfsetispeed(&tty, (speed_t)spd);

    cfmakeraw(&tty);

    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 10;

    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;    /* no HW flow control? */
    tty.c_cflag |= CLOCAL | CREAD;
    rc = tcsetattr(sfd, TCSANOW, &tty);
    if (rc < 0) {
        printf("Issue in setting attributes\n");
                return (-3);
    }
    return sfd;
}

int main(int argc, char *argv[])
{
	char *usb_dev = NULL;
	config_setting_t pod_config;
	char ret;
	char pwd[4];

	if(argc > 1)
		usb_dev = argv[1];
	else
	{
		printf("Provide usb port\n");
		return 0;
	}
	printf("USB device is:%s\n", usb_dev);

	usb_fd = serialInit(usb_dev);
	ret = get_config(&pod_config);
	if(ret == -1)
		printf("Read config failed\n");

	d_config = (struct config_data_t *)&pod_config.arg;
	memcpy(pwd, d_config->cDevPwd, 4);

	printf("Device configuration received\n");
	printf("Device config valid:%d\n", d_config->valid);
	printf("Device sTID:%s uTID:%s\n", d_config->cSonicTid, d_config->cUSonicTid);
	printf("Device ID:0x%x 0x%x 0x%x 0x%x\n",d_config->cDevNo[0], d_config->cDevNo[1], d_config->cDevNo[2], d_config->cDevNo[3] );
	printf("Device Pwd:%s\n", pwd);
	printf("Device Config Version:%d\n", d_config->cConfigVersion);
	printf("Device FW version:%s\n", d_config->cFwVersion);


	if(d_config->cConfigVersion == 0)
	{
		printf("Device first time configuration required\n");
		printf("Update TID and features of the device\n");
	}
PWD_ENTER:
	printf("Please enter device password to update configuration\n");
	scanf("%s", pwd);
	if(!strncmp(pwd, (const char *)d_config->cDevPwd, 4))
	{
		printf("Password correct\n");
		set_sonic_TID(&pod_config, TID_DATA);
		set_usonic_TID(&pod_config, TID_DATA);
		set_dev_pwd(&pod_config, "1234");
		set_config_version(&pod_config);
		set_burst_enable_on_usb(&pod_config, DISABLE, 0);

		/* Enable or Disable the feature which are required */
		set_enable_tap(&pod_config, ENABLE);
		set_enable_confimration(&pod_config, ENABLE);
		set_enable_fixed_amount_mode(&pod_config, DISABLE, 0);
		set_enable_IVR(&pod_config, DISABLE);
		set_enable_wifi(&pod_config, DISABLE);

		printf("Length of config:%d\n", pod_config.length);
		write_config(&pod_config);
	}
	else
	{
		printf("Password incorrect\n");
		goto PWD_ENTER;
	}

	d_config = (struct config_data_t *)&pod_config.arg[0];
	memcpy(pwd, d_config->cDevPwd, 4);

	printf("Device configuration received\n");
	printf("Device config valid:%d\n", d_config->valid);
	printf("Device sTID:%s uTID:%s\n", d_config->cSonicTid, d_config->cUSonicTid);
	printf("Device ID:0x%x 0x%x 0x%x 0x%x\n",d_config->cDevNo[0], d_config->cDevNo[1], d_config->cDevNo[2], d_config->cDevNo[3] );
	printf("Device Pwd:%4s\n", pwd);
	printf("Device Config Version:%d\n", d_config->cConfigVersion);
	printf("Device FW version:%s\n", d_config->cFwVersion);
	printf("Device features: BurstEnable:%d TapEnable:%d COnfirmationEnable:%d\n", d_config->cBurstEnableOnUSB, d_config->cEnableTap, d_config->cEnableConfirm);
	printf("Total Txn:%d Days Txn:%d\n", d_config->dTotalTxnAmnt, d_config->dDaysTxnAmnt);
}

